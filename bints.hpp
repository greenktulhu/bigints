#include <string>
#include <sstream>

typedef struct
{
    std::string value;
    bool is_positive;
} bint;

bool is_valid_bint (const bint &n);

bint trim_bint (const bint &n);

std::string bint_to_str (const bint &n);

bint to_bint (const int &i);

bint to_bint (const std::string &s);

bint abs_bint (const bint &n);

bool bigger_bint (const bint &a, const bint &b);

bint add_bint (const bint &a, const bint &b);

bint subtract_bint (const bint &a, const bint &b);

bool
is_valid_bint (const bint &n)
{
    if (n.value == "")
        {
            return false;
        }
    for (const char c : n.value)
        {
            if (c == '0')
                continue;
            if (c == '1')
                continue;
            if (c == '2')
                continue;
            if (c == '3')
                continue;
            if (c == '4')
                continue;
            if (c == '5')
                continue;
            if (c == '6')
                continue;
            if (c == '7')
                continue;
            if (c == '8')
                continue;
            if (c == '9')
                continue;
            return false;
        }
    return true;
}

bint
trim_bint (const bint &n)
{
    if (n.value == "0")
        {
            return { n.value, 1 };
        }
    if (n.value.size () == 1)
        {
            return n;
        }

    int zero_index = -1;
    std::string v_to_check = n.value.substr (0, n.value.size () - 1);
    for (char c : v_to_check)
        {
            if (c == '0')
                {
                    zero_index++;
                    continue;
                }
            break;
        }

    if (zero_index != -1)
        {
            int start_i = zero_index + 1;
            int len = n.value.size () - start_i;
            std::string ret_value = n.value.substr (start_i, len);
            return { ret_value, n.is_positive };
        }

    return n;
}

std::string
bint_to_str (const bint &n)
{
    if (n.is_positive)
        {
            return n.value;
        }
    else
        {
            return "-" + n.value;
        }
}

bint
to_bint (const int &i)
{
    std::stringstream stream;
    int v = i;
    bool is_pos;
    if (i < 0)
        {
            is_pos = 0;
            v = abs (v);
        }
    else
        {
            is_pos = 1;
        }
    stream << v;
    return trim_bint ({ stream.str (), is_pos });
}

bint
to_bint (const std::string &s)
{
    bool is_pos;
    std::string v;
    if (s.at (0) == '-')
        {
            is_pos = 0;
            v = s.substr (1, s.size () - 1);
        }
    else
        {
            is_pos = 1;
            v = s;
        }
    bint ret = { v, is_pos };
    assert (is_valid_bint (ret));
    return trim_bint (ret);
}

bint
abs_bint (const bint &n)
{
    if (n.is_positive)
        {
            return n;
        }
    else
        {
            return { n.value, 1 };
        }
}

bool
bigger_bint (const bint &a, const bint &b) // true if a > b
{
    if ((a.is_positive == b.is_positive) && (a.value == b.value))
        {
            return false;
        }
    if (a.is_positive && !b.is_positive)
        {
            return true;
        }
    if (b.is_positive && !a.is_positive)
        {
            return false;
        }

    bool both_negative = !a.is_positive && !b.is_positive;
    size_t len_a = a.value.size ();
    size_t len_b = b.value.size ();
    if (len_a > len_b)
        {
            if (both_negative)
                {
                    return false;
                }
            else
                {
                    return true;
                }
        }
    else if (len_a < len_b)
        {
            if (both_negative)
                {
                    return true;
                }
            else
                {
                    return false;
                }
        }
    else // len_a == len_b
        {
            for (size_t i = 0; i < len_a; i++)
                {
                    int ia = a.value.at (i) - '0';
                    int ib = b.value.at (i) - '0';
                    if (ia == ib)
                        {
                            continue;
                        }
                    else if (ia > ib)
                        {
                            return !both_negative ? true : false;
                        }
                    else if (ia < ib)
                        {
                            return both_negative ? true : false;
                        }
                }
        }
    return false;
}

bint
add_bint (const bint &a, const bint &b)
{
    if (!a.is_positive && !b.is_positive)
        {
            return { add_bint (abs_bint (a), abs_bint (b)).value, 0 };
        }

    else if (a.is_positive && !b.is_positive)
        {
            return subtract_bint (a, abs_bint (b));
        }

    else if (!a.is_positive && b.is_positive)
        {
            return subtract_bint (b, abs_bint (a));
        }

    else if (a.is_positive && b.is_positive)
        {
            std::string ret = "";
            std::string a_value = a.value;
            std::string b_value = b.value;

            int len_diff = a_value.size () - b_value.size ();
            if (len_diff < 0)
                {
                    int i = abs (len_diff);
                    while (i > 0)
                        {
                            a_value = "0" + a_value;
                            i--;
                        }
                }
            else if (len_diff > 0)
                {
                    int i = abs (len_diff);
                    while (i > 0)
                        {
                            b_value = "0" + b_value;
                            i--;
                        }
                }
            assert (a_value.size () == b_value.size ());

            int carry = 0;
            int index_addition = a_value.size () - 1;
            while (index_addition >= 0)
                {
                    int a_digit = a_value.at (index_addition) - '0';
                    int b_digit = b_value.at (index_addition) - '0';
                    int digits_sum = a_digit + b_digit + carry;

                    carry = digits_sum / 10;
                    digits_sum = digits_sum % 10;

                    ret = std::to_string (digits_sum) + ret;

                    index_addition--;
                }

            ret = std::to_string (carry) + ret;

            return to_bint (ret);
        }
    assert (0);
}

bint
subtract_bint (const bint &a, const bint &b)
{
    if (!a.is_positive && !b.is_positive)
        {
            return subtract_bint (abs_bint (b), abs_bint (a));
        }
    else if (a.is_positive && !b.is_positive)
        {
            return add_bint (a, abs_bint (b));
        }
    else if (!a.is_positive && b.is_positive)
        {
            return { add_bint (abs_bint (a), b).value, 0 };
        }
    else if (a.is_positive && b.is_positive)
        {
            if (bigger_bint (b, a))
                {
                    return { subtract_bint (b, a).value, 0 };
                }
            else
                {
                    std::string ret = "";
                    std::string a_value = a.value;
                    std::string b_value = b.value;

                    int len_diff = a_value.size () - b_value.size ();
                    if (len_diff < 0)
                        {
                            int i = abs (len_diff);
                            while (i > 0)
                                {
                                    a_value = "0" + a_value;
                                    i--;
                                }
                        }
                    else if (len_diff > 0)
                        {
                            int i = abs (len_diff);
                            while (i > 0)
                                {
                                    b_value = "0" + b_value;
                                    i--;
                                }
                        }
                    assert (a_value.size () == b_value.size ());

                    int carry = 0;
                    int index_addition = a_value.size () - 1;
                    while (index_addition >= 0)
                        {
                            int a_digit = a_value.at (index_addition) - '0';
                            int b_digit = b_value.at (index_addition) - '0';
                            int digits_diff = a_digit - b_digit + carry;

                            if (digits_diff < 0)
                                {
                                    digits_diff += 10;
                                    carry = -1;
                                }
                            else
                                {
                                    carry = 0;
                                }

                            ret = std::to_string (digits_diff) + ret;

                            index_addition--;
                        }

                    return to_bint (ret);
                }
        }
    assert (0);
}