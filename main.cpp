#include <iostream>
#include <cassert>
#include "bints.hpp"

#include <cstdlib> 
#include <ctime> 

int main(int argc, char const *argv[])
{
  srand((unsigned)time(0)); 
  bint num = to_bint(0);
  for (size_t i = 0; i < 999999; i++)
  {
    num = subtract_bint(num, to_bint(rand()));
  }
  std::cout << bint_to_str(num) << '\n';
  
  // if (argc != 3)
  // {
  //   return 1;
  // }
  // bint v1 = to_bint(argv[1]);
  // bint v2 = to_bint(argv[2]);
  // std::cout << bint_to_str(v1) << "+" << bint_to_str(v2) << "=" << bint_to_str(add_bint(v1, v2)) << "\n";
}
