# bigints

Functions to manipulate big integers.


# Usage

## create bint object:
* `to_bint(int or std::string)` -> `bint`
#### or
* **initialize** with `{value_string, is_positive}`

## Add
* `add_bint(a, b)` -> `bint`
## Subtract
* `subtract_bint(a, b)` -> `bint`